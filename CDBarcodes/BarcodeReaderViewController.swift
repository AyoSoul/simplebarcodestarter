//
//  BarcodeReaderViewController.swift
//  CDBarcodes
//
//  Created by Matthew Maher on 1/29/16.
//  Copyright © 2016 Matt Maher. All rights reserved.
//

import UIKit
import AVFoundation

class BarcodeReaderViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var session : AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        session = AVCaptureSession()
        let videoCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        let videoInput: AVCaptureDeviceInput?
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice!)
        } catch {
            return
        }
        
        if (session.canAddInput(videoInput!)) {
            session.addInput(videoInput!)
        } else {
            scanningNotPossible()
        }
        
        // Create output object
        let metadataOutput = AVCaptureMetadataOutput()
        
        // Add output to the session
        if (session.canAddOutput(metadataOutput)) {
            session.addOutput(metadataOutput)
            
            // Send captured data to the delegate object via a serial queue.
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            // Set barcode type for which to scan: EAN-13
            metadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.ean13]
            
        } else {
            scanningNotPossible()
        }

        // Add previewLayer and have it show the video data
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        // Begin the capture session
        session.startRunning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !(session!.isRunning){
            session.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (session.isRunning) {
            session.stopRunning()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func scanningNotPossible() {
        let alert = UIAlertController(title: "Can't scan", message: "Let's try a device equipped with a camera.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        session = nil
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!){
        
        // Get the first object from the metadataObjects array
        if let barcodeData = metadataObjects.first {
            // Turn it into machine readable code
            let bacodeReadable = barcodeData as? AVMetadataMachineReadableCodeObject
            if let readableCode = bacodeReadable {
                barcodeDetected(readableCode.stringValue!)
            }
            
            // Vibrate the device to give the user some feedback
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
            // Aviod a very buzzy device
            session.stopRunning()
        }
    }
    
    func barcodeDetected(_ code : String) {
        // Let the user know we have found something
        let alert = UIAlertController(title: "Found a Barcode", message: code, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Search", style: UIAlertAction.Style.destructive, handler: { action in
            
            // Remove the spaces
            let trimmedCode = code.trimmingCharacters(in: NSCharacterSet.whitespaces)
            
            // EAN or UPC
            // Check for added "0" at beginning of code
            
            let trimmedCodeString = "\(trimmedCode)"
            var trimmedCodeNoZero : String
            
            if trimmedCodeString.hasPrefix("0") && String(trimmedCodeString).count > 1 {
                trimmedCodeNoZero = String(String(trimmedCodeString).dropFirst())
                
                // Send the doctored UPC to DataService.searchAPI()
                DataService.searchAPI(trimmedCodeNoZero)
                print(trimmedCodeNoZero)
            } else {
                // Send the doctored EAN to DataService.searchAPI()
                DataService.searchAPI(trimmedCodeString)
                print(trimmedCodeString)
            }
            
            self.navigationController?.popViewController(animated: true)
        }))
        
        present(alert, animated: true, completion: nil)
    }
}
